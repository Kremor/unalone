from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core import blocks
from wagtail.core.fields import StreamField
from wagtail.core.models import Page
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.blocks import ImageChooserBlock


class HomePage(Page):

    def get_context(self, request, *args, **kwargs):
        # Update context to include only published posts, ordered by reverse-chron
        context = super().get_context(request)
        posts = BasicPage.objects.all().live().order_by('-first_published_at')
        context['posts'] = posts
        return context


class BasicPage(Page):
    subtitle = models.CharField(max_length=500, default='Subtitle', blank=False)
    summary = models.TextField(default='Summary', blank=False)
    cover = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=False,
        on_delete=models.SET_NULL,
        related_name='+'''
    )
    body = StreamField([
        ('embed', EmbedBlock()),
        ('image', ImageChooserBlock()),
        ('quote', blocks.BlockQuoteBlock(classname='quote', max_length=500)),
        ('raw_html', blocks.RawHTMLBlock()),
        ('text', blocks.RichTextBlock()),
    ], null=True)

    content_panels = Page.content_panels + [
        FieldPanel('subtitle'),
        FieldPanel('summary'),
        ImageChooserPanel('cover'),
        StreamFieldPanel('body'),
    ]


class BlankPage(Page):
    pass
