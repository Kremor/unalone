from django import template
from django.core.exceptions import ObjectDoesNotExist


register = template.Library()


@register.simple_tag(takes_context=True)
def get_children(context):
    item = context['item']
    children = []
    try:
        children = item.get_children().live().in_menu()
    except ObjectDoesNotExist:
        pass
    print(children)
    return children


@register.inclusion_tag(takes_context=True, filename="desktop_menu.html")
def top_menu(context):
    root = context['request'].site.root_page
    menu_items = root.get_children().live().in_menu()
    return {'menu_items': menu_items}
