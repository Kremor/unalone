const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')


module.exports = {
    entry: {
        editor: path.resolve(__dirname, 'js/editor.js'),
        index: path.resolve(__dirname, 'js/index.js'),
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    resolve: {
        alias: {
            img: path.resolve(__dirname, 'img'),
            'vue$': 'vue/dist/vue.esm.js'
        },
        extensions: ['.js', '.json', '.vue']
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    module: {
        rules: [
            {
              test: /\.vue$/,
              exclude: /node_modules/,
              use: 'vue-loader'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'stage-3']
                    }
                }
            },
            {
                test: /\.s?css$/,
                exclude: /node_modules/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.jpg$/,
                exclude: /node_modules/,
                use: ['file-loader']
            }
        ]
    },
    mode: 'development'
}